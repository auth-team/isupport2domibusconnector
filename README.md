# iSupport2DomibusConnector

## HOW TO BUILD

```bash
mvn clean install
```

Distributions are in target/dist folder


## HOW TO RUN

1. Configure application.properties
2. Run in console: ``java -jar iSupport2DomibusConnector.jar``