/*
 * Copyright (c) 2019.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package gr.auth.ecodex.iSupport2DomibusConnector.scanner;

import gr.auth.ecodex.iSupport2DomibusConnector.models.StandardBusinessDocumentHeader;
import gr.auth.ecodex.iSupport2DomibusConnector.util.InjectedProperties;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

@Component
public class ISupport2DomibusImpl implements ISupport2Domibus {

    private InjectedProperties injectedProperties;
    private Logger log = LoggerFactory.getLogger(ISupport2DomibusImpl.class);

    /**
     * Constructor, basically does nothing, just logs
     * @param injectedProperties application properties
     */
    public ISupport2DomibusImpl(@NotNull InjectedProperties injectedProperties) {
        this.injectedProperties = injectedProperties;
        log.debug("ISupport2DomibusImpl Constructed");
        log.debug("DomibusIncomingDir: " + injectedProperties.getDomibusIncomingDir());
        log.debug("DomibusOutgoingDir: " + injectedProperties.getDomibusOutgoingDir());
        log.debug("iSupportIncomingDir: " + injectedProperties.getiSupportIncomingDir());
        log.debug("iSupportOutgoingDir: " + injectedProperties.getiSupportOutgoingDir());
    }

    /**
     * Main method for scanning iSupport outgoing folders for new messages
     */
    @Override
    public void scanISupportOutgoing() {
        log.debug("Start iSupport folder scan");
        log.debug("Scanning isupport OutgiongDir " + injectedProperties.getiSupportOutgoingDir());

        if (!directoriesExists()) {
            log.error("Problem with directories.");
            return;
        }

        File[] outgoing = getListOfFolders(injectedProperties.getiSupportOutgoingDir());

//        File[] outgoingWithoutEvidence = filterFoldersWithoutEvidences(outgoing);
        for (File f : outgoing) {
            checkForEvidences(f);
        }

        File[] new_outgoing = filterWithNoSuffix(outgoing, injectedProperties.getProcessedSuffix());
        for (File f : new_outgoing) {
            processISupportOutgoing(f);
        }
    }

    /**
     * Main method for scanning Connector folders for new incoming messages
     */
    @Override
    public void scanDomibusConnectorIncoming() {
        log.debug("Start Connector folder scan");
        log.debug("Scanning domibus IncomingDir " + injectedProperties.getDomibusIncomingDir());

        if (!directoriesExists()) {
            log.error("Problem with directories.");
            return;
        }

        File[] incoming = getListOfFolders(injectedProperties.getDomibusIncomingDir());
        File[] new_incoming = filterWithNoSuffixFile(incoming, injectedProperties.getProcessedSuffix());

        for (File f : new_incoming) {
            processDomibusIncoming(f);
        }
    }

    /**
     * This method filters all folders that contains a suffix in the dir name
     * @param dirs folders to filter
     * @param suffix suffix to find
     * @return filtered folders
     */
    @NotNull
    private File[] filterWithNoSuffix(@NotNull File[] dirs, String suffix) {
        List<File> dirsFiltered = new ArrayList<>();
        for (File dir : dirs) {
            if (!dir.getName().endsWith(suffix)) {
                dirsFiltered.add(dir);
            }
        }
        return dirsFiltered.toArray(new File[0]);
    }

    /**
     * This method filters all folders that contain a file with a name as suffix
     * @param dirs folders to filter
     * @param suffix file to find
     * @return filtered folders
     */
    @NotNull
    private File[] filterWithNoSuffixFile(@NotNull File[] dirs, String suffix){
        List<File> dirs_filterd = new ArrayList<>();
        for (File dir : dirs) {
            File testFile = new File(dir.getAbsolutePath() + File.separator + suffix);
            String messageFile = dir.getAbsolutePath() + File.separator + "message.properties";
            String service;
            try{
                service = readProperty(messageFile, "service");
            } catch (IOException e){
                log.error(e.getMessage());
                continue;
            }
            if (!testFile.exists() & service.equals(injectedProperties.getService())) {
                dirs_filterd.add(dir);
            }
        }
        return dirs_filterd.toArray(new File[0]);
    }

    /**
     * Get the list of all folders under path
     * @param path path to search
     * @return list of folders found
     */
    private File[] getListOfFolders(String path) {
        File file = new File(path);
        File[] files = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        if (files != null) {
            log.trace("Directories under: " + path);
            for (File dir : files) {
                log.trace("Message: " + dir.getAbsolutePath());
            }
        } else {
            return new File[0];
        }
        return files;
    }

    /**
     * Check if main folders exists
     * @return true if all message folders in application.properties exists
     */
    private boolean directoriesExists() {
        if (!new File(injectedProperties.getiSupportIncomingDir()).exists()) {
            log.error("Folder does not exists: " + injectedProperties.getiSupportIncomingDir());
            return false;
        }
        if (!new File(injectedProperties.getiSupportOutgoingDir()).exists()) {
            log.error("Folder does not exists: " + injectedProperties.getiSupportOutgoingDir());
            return false;
        }
        if (!new File(injectedProperties.getDomibusIncomingDir()).exists()) {
            log.error("Folder does not exists: " + injectedProperties.getDomibusIncomingDir());
            return false;
        }
        if (!new File(injectedProperties.getDomibusOutgoingDir()).exists()) {
            log.error("Folder does not exists: " + injectedProperties.getDomibusOutgoingDir());
            return false;
        }
        return true;
    }

    /**
     *
     * @param dir message folder
     */
    private void processISupportOutgoing(@NotNull File dir) {
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        log.info("Start Processing new outgoing message: " + dir.getName());
        try {
            //
            // Copy message folder into domibus outgoing folder
            //
            log.info("Copying message dir into: " + injectedProperties.getDomibusOutgoingDir());
            File destDir = new File(injectedProperties.getDomibusOutgoingDir() + dir.getName() + "_message");
            FileUtils.copyDirectory(dir, destDir);

            //
            // Create empty pdf
            //
            if (injectedProperties.isAddPdf()) {
                try {
                    Resource sourceFile = new ClassPathResource("empty.pdf");
                    String newFileName = destDir.getAbsolutePath() + File.separator + injectedProperties.getPdfName();
                    File destinationFile = new File(newFileName);
                    FileUtils.copyInputStreamToFile(sourceFile.getInputStream(), destinationFile);
                    log.trace("Copy PDF File OK");
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }

            //
            // Read StandardBusinessDocumentHeader from header.xml
            //
            String filename = dir.getAbsolutePath() + File.separator + injectedProperties.getHeaderXML();
            File xmlFile = new File(filename);
            StandardBusinessDocumentHeader sbd = getXML(xmlFile);
            log.trace("Receiver Identifier" + sbd.getTransport().getReceiver().getIdentifier());

            //
            // Create message.properties file
            //
            String messageFile = destDir.getAbsolutePath() + File.separator + "message.properties";
            createMessgeProps(messageFile, sbd, dir.getName() + injectedProperties.getMessageIDSuffix());

            //
            // Rename iSupport message folder bya adding suffix
            //
            String newDirName = dir.getAbsolutePath() + injectedProperties.getProcessedSuffix();
            log.info("Renaming message dir into:" + newDirName);
            File newDir = new File(newDirName);
            if (dir.renameTo(newDir)) {
                log.trace("Dir rename OK");
            }
        } catch (IOException | JAXBException e) {
            log.error(e.getMessage());
        }
        log.info("Message digested: " + dir.getName());
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    }

    /**
     *
     * @param dir message folder
     */
    private void processDomibusIncoming(@NotNull File dir) {
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        log.info("Start Processing new incoming message: " + dir.getName());
        try {
            log.info("Renaming Form_A.xml");

            String messageFile = dir.getAbsolutePath() + File.separator + "message.properties";
            String headerXML = dir.getAbsolutePath() + File.separator + injectedProperties.getHeaderXML();
            try {
                String formFile = readProperty(messageFile, "content.xml.file.name");
                String formAXML = dir.getAbsolutePath() + File.separator + formFile;
                FileUtils.copyFile(new File(formAXML), new File(headerXML));
            } catch (IOException e) {
                log.error(e.getMessage());
            }

            log.info("Copying message dir into: " + injectedProperties.getiSupportIncomingDir());

            StandardBusinessDocumentHeader sbd = getXML(new File(headerXML));
            String prefix = "Message" + sbd.getTransport().getMsgId() +
                    "_" + sbd.getTransport().getSender().getIdentifier() +
                    "_" + sbd.getTransport().getReceiver().getIdentifier() + "_";
            String newDir = prefix + dir.getName();

            File destDir = new File(injectedProperties.getiSupportIncomingDir() + newDir);
            FileUtils.copyDirectory(dir, destDir);

            // Filter files by extension
            filterFiles(destDir);

            String newFileName = dir.getAbsolutePath() + File.separator + injectedProperties.getProcessedSuffix();
            log.info("Creating check file: " + newFileName);
            File newFile = new File(newFileName);
            if (newFile.createNewFile()) {
                log.trace("Create New File OK");
            }
        } catch (IOException | JAXBException e) {
            log.error(e.getMessage());
        }
        log.info("Message digested: " + dir.getName());
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    }

    /**
     * Delete filtered files inside a directory
     *
     * @param dir message folder
     */
    private void filterFiles(File dir) {
        if (!dir.exists()) {
            return;
        }
        Collection<File> files = FileUtils.listFiles(dir, null, false);
        for (File file : files) {
            if (file.isFile()) {
                String ext = FilenameUtils.getExtension(file.getName());
                if (!injectedProperties.getAllowedExtensions().contains(ext)) {
                    log.info("Deleting filtered file: " + file.getName());
                    FileUtils.deleteQuietly(file);
                }
            }
        }
    }

    /**
     * Read the xml file
     * @param file xml file
     * @return object from xml file
     * @throws JAXBException error on read xml
     * @throws IOException error on read file
     */
    private StandardBusinessDocumentHeader getXML(@NotNull File file) throws JAXBException, IOException {
        if (!file.exists()) {
            throw new IOException("XML File not found");
        }
        JAXBContext jaxbContext = JAXBContext.newInstance(StandardBusinessDocumentHeader.class);
        Unmarshaller jaxbMarshaller = jaxbContext.createUnmarshaller();

        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        if (fileInputStream.read(data) == 0) {
            log.trace("Data read OK");
        }
        fileInputStream.close();

        return (StandardBusinessDocumentHeader) jaxbMarshaller.unmarshal(new
                ByteArrayInputStream(new String(data).getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * Creates the message.properties file
     * @param messageFile properties file name
     * @param sbd xml object
     * @param messageID name of folder after rename by connector
     * @throws IOException error on write file
     */
    private void createMessgeProps(String messageFile, StandardBusinessDocumentHeader sbd,
                                   String messageID) throws IOException {
        Properties props = new Properties();
        props.setProperty("service", injectedProperties.getService());
        props.setProperty("action", injectedProperties.getAction());
        props.setProperty("national.message.id", messageID);
        props.setProperty("original.sender", sbd.getTransport().getSender().getContactInformation().getContact());
        props.setProperty("final.recipient", sbd.getTransport().getReceiver().getContactInformation().getContact());
        props.setProperty("from.party.id", sbd.getTransport().getSender().getIdentifier());
        props.setProperty("from.party.role", injectedProperties.getFromPartyRole());
        props.setProperty("to.party.id", sbd.getTransport().getReceiver().getIdentifier());
        props.setProperty("to.party.role", injectedProperties.getToPartyRole());
        props.setProperty("conversation.id", sbd.getTransport().getCaseId());
        if (injectedProperties.isAddPdf())
            props.setProperty("content.pdf.file.name", injectedProperties.getPdfName());
        props.setProperty("content.xml.file.name", injectedProperties.getHeaderXML());
        try(FileOutputStream stream = new FileOutputStream(new File(messageFile))) {
            props.store(stream, "Message Properties");
        }
    }

    /**
     * Read a value from a prop file given a key
     * @param propFile properties file name
     * @param key key name
     * @return value
     * @throws IOException error on
     */
    private String readProperty(String propFile, String key) throws IOException{
        Properties prop = new Properties();
        try(FileInputStream stream = new FileInputStream(propFile)) {
            prop.load(stream);
        }
        return prop.getProperty(key);
    }

    /**
     * Filters folders wich contains evidences
     * @param dirs folders to filter
     * @return filtered folders
     */
    private File[] filterFoldersWithoutEvidences(@NotNull File[] dirs) {
        log.debug("Filtering folders for evidences");
        List<File> dirsFiltered = new ArrayList<>();
        for (File dir : dirs) {
            File ev1 = new File(dir.getAbsolutePath() + File.separator + "SUBMISSION_ACCEPTANCE.xml");
            File ev2 = new File(dir.getAbsolutePath() + File.separator + "SUBMISSION_REJECTION.xml");
            if (!ev1.exists() & !ev2.exists()) {
                log.trace("folder with no evidences found " + dir.getName());
                dirsFiltered.add(dir);
            }
        }
        return dirsFiltered.toArray(new File[0]);
    }

    /**
     * Check for evidences in the connector and copy them in the iSupport
     * @param dir message folder
     */
    private void checkForEvidences(@NotNull File dir) {
        log.trace("checking for evidences " + dir.getName());

        String dirName = dir.getName();
        if (dirName.endsWith(injectedProperties.getProcessedSuffix())) {
            dirName = dirName.split(injectedProperties.getProcessedSuffix())[0];
        }

        for (String evidenceName : injectedProperties.getEvidences()) {
            File destFile = new File(dir.getAbsolutePath() + File.separator + evidenceName);
            if (destFile.exists()) {
                log.debug("Evidence already there " + destFile.getAbsolutePath());
                continue;
            }
            String evidenceFileName = injectedProperties.getDomibusOutgoingDir() + File.separator +
                    dirName + injectedProperties.getMessageIDSuffix() + "_sent" + File.separator +
                    evidenceName;
            File evidence = new File(evidenceFileName);
            if (evidence.exists()) {
                log.info("Found evidence for message : " + dirName + " evidence: " + evidenceName);
                log.trace("Copying evidences " + evidenceName);
                try {
                    FileUtils.copyFile(evidence, destFile);
                } catch (IOException e) {
                    log.error("Could not copy evidence: " + evidenceFileName);
                    log.error(e.getMessage());
                }
            } else {
                log.debug("Not found evidence yet " + evidenceFileName);
            }
        }

    }
}
