/*
 * Copyright (c) 2019.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package gr.auth.ecodex.iSupport2DomibusConnector.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * Injected properties from application.properties file
 */
@Configuration
@ConfigurationProperties("scanner")
public class InjectedProperties {

    private String domibusIncomingDir;
    private String domibusOutgoingDir;
    private String iSupportIncomingDir;
    private String iSupportOutgoingDir;
    private String processedSuffix;
    private String evidencesString;
    private String[] evidences;
    private String messageIDSuffix;
    private String service;
    private String action;
    private String headerXML;
    private String pdfName;
    private boolean addPdf;
    private List<String> allowedExtensions;
    private String allowedExtensionsString;
    private String fromPartyRole;
    private String toPartyRole;

    public String getAllowedExtensionsString() {
        return allowedExtensionsString;
    }

    public void setAllowedExtensionsString(String allowedExtensionsString) {
        this.allowedExtensionsString = allowedExtensionsString;
        setAllowedExtensions(Arrays.asList(allowedExtensionsString.split(",")));
    }

    public List<String> getAllowedExtensions() {
        return allowedExtensions;
    }

    public void setAllowedExtensions(List<String> allowedExtensions) {
        this.allowedExtensions = allowedExtensions;
    }

    public String getHeaderXML() {
        return headerXML;
    }

    public void setHeaderXML(String headerXML) {
        this.headerXML = headerXML;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public boolean isAddPdf() {
        return addPdf;
    }

    public void setAddPdf(boolean addPdf) {
        this.addPdf = addPdf;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String[] getEvidences() {
        return evidences;
    }

    public void setEvidences(String[] evidences) {
        this.evidences = evidences;
    }

    public String getEvidencesString() {
        return evidencesString;
    }

    public void setEvidencesString(String evidencesString) {
        this.evidencesString = evidencesString;
        setEvidences(evidencesString.split(","));
    }

    public String getMessageIDSuffix() {
        return messageIDSuffix;
    }

    public void setMessageIDSuffix(String messageIDSuffix) {
        this.messageIDSuffix = messageIDSuffix;
    }

    public String getProcessedSuffix() {
        return processedSuffix;
    }

    public void setProcessedSuffix(String processedSuffix) {
        this.processedSuffix = processedSuffix;
    }

    public String getiSupportOutgoingDir() {
        return iSupportOutgoingDir;
    }

    public void setiSupportOutgoingDir(String iSupportOutgoingDir) {
        this.iSupportOutgoingDir = iSupportOutgoingDir;
    }

    public String getDomibusIncomingDir() {
        return domibusIncomingDir;
    }

    public void setDomibusIncomingDir(String domibusIncomingDir) {
        this.domibusIncomingDir = domibusIncomingDir;
    }

    public String getDomibusOutgoingDir() {
        return domibusOutgoingDir;
    }

    public void setDomibusOutgoingDir(String domibusOutgoingDir) {
        this.domibusOutgoingDir = domibusOutgoingDir;
    }

    public String getiSupportIncomingDir() {
        return iSupportIncomingDir;
    }

    public void setiSupportIncomingDir(String iSupportIncomingDir) {
        this.iSupportIncomingDir = iSupportIncomingDir;
    }

    public String getFromPartyRole() {
        return fromPartyRole;
    }

    public void setFromPartyRole(String fromPartyRole) {
        this.fromPartyRole = fromPartyRole;
    }

    public String getToPartyRole() {
        return toPartyRole;
    }

    public void setToPartyRole(String toPartyRole) {
        this.toPartyRole = toPartyRole;
    }
}
